# SPDX-FileCopyrightText: 2023 David Runge <dvzrv@archlinux.org>
# SPDX-License-Identifier: CC0-1.0

stages:
  - check
  - build
  - test
  # - publish

variables:
  CARGO_HOME: "${CI_PROJECT_DIR}/.cargo"

cache:
  paths:
  - .cargo/
  - target/

.prepare:
  before_script:
    - pacman -Sy --needed --noconfirm archlinux-keyring
    - pacman -Syu --needed --noconfirm cargo-deny cocogitto codespell reuse rust

check:
  extends: .prepare
  stage: check
  needs: []
  script:
    - ./.ci/check.sh
    - cog check

build:
  extends: .prepare
  stage: build
  needs:
    - check
  artifacts:
    paths:
      - target
    expire_in: 1 day
  script:
    - cargo build --release

test:
  extends: .prepare
  stage: test
  needs:
    - build
  script:
    - cargo test --release

# we do not publish in CI, because the tokens have no scopes and that is dangerous:
# https://github.com/rust-lang/crates.io/issues/5443
# crates_publish:
#   extends: .prepare
#   needs:
#     - build
#     - check
#     - test
#   rules:
#     - if: '$CARGO_REGISTRY_TOKEN && $CI_COMMIT_TAG && $CI_PROJECT_PATH == "archlinux/alpm/alpm-types"'
#   stage: publish
#   tags:
#     - secure
#   script:
#     - cargo package
#     - cargo publish
