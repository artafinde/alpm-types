<!--
SPDX-FileCopyrightText: 2023 David Runge <dvzrv@archlinux.org>
SPDX-License-Identifier: CC-BY-SA-4.0
-->
- - -
## 0.1.0 - 2023-04-04
#### Continuous Integration
- Add check scripts and Gitlab CI integration - (a301b04) - David Runge
#### Documentation
- correct path for quick-check.sh - (06c36ee) - Leonidas Spyropoulos
#### Features
- Limit chrono features to avoid audit RUSTSEC-2020-0071 - (a32127f) - Leonidas Spyropoulos
- Implement Md5sum type - (6ab68a8) - Leonidas Spyropoulos
- Increase MSRV to 1.60.0 - (150c878) - David Runge
- Implement Name type - (335d13c) - David Runge
- Implement PkgType - (540746d) - David Runge
- Use rstest to parametrize tests - (44b7644) - David Runge
- Use thiserror to remove Error boilerplate - (14620dd) - David Runge
- Replace enum boilerplate with strum - (d6fc661) - David Runge
- Add initial types (Architecture, BuildDate, CompressedSize, InstalledSize) - (2deba0f) - David Runge
#### Miscellaneous Chores
- Publish to crates.io locally (not from CI) - (a0e6b54) - David Runge
- Change CI scripts to LGPL-3.0-or-later - (8995c51) - David Runge

- - -

